// Timers
const int timeWork = 25; // Length of work period in minutes
const int timeShortBreak = 5; // Length of short break period in minutes

// Alarm config
const int pinSpeaker = 12; // Pin number to connect piezo speaker
const int beepFrequencyWork = 523; // Frequency of beeps on work period start
const int beepFrequencyBreak = 349; // Frequency of beeps on break period start

// Bonus break
// Add random extra break time to short break period.
const int enableBonusBreak = 0; // 1 for enable bonus break time in Short break period

// Reward
// Play a special chime at certain odds. You can use this to reward yourself.
const int enableReward = 0; // 1 for enable Reward chime
const int rateReward = 100; // Probability of reward e.g. 100 for 1% (1 out of 100)

// Long break period
// Insert a longer break in every 4 phases (Work period and Short break period).
const int enableLongBreak = 1; // 1 for enable Long break period
const int timeLongBreak = 30; // Length of Long break period in minutes

void setup() {
  randomSeed(analogRead(0));
}

void loop() {
  for (int i = 0; i <= 3; i++){
    // Work period
    digitalWrite(LED_BUILTIN, HIGH);
    tone(pinSpeaker,beepFrequencyWork,500);
    delay(1000UL*60*timeWork);
    
    // Short break period
    digitalWrite(LED_BUILTIN, LOW);
    tone(pinSpeaker,beepFrequencyBreak,100);
    delay(300);
    tone(pinSpeaker,beepFrequencyBreak,100);
    delay(1000);
      // Reward
      if (enableReward == 1){
      int lottery = random(rateReward);
        if (lottery == 0) {
          tone(pinSpeaker,262,100);
          delay(300);
          tone(pinSpeaker,330,100);
          delay(300);
          tone(pinSpeaker,392,100);
          delay(300);
          tone(pinSpeaker,523,100);
          delay(1000);
      }
    }
    delay(1000UL*60*timeShortBreak);

      // Bonus Break
    if (enableBonusBreak == 1) {
      int timeBonusBreak = random(timeShortBreak/2);
      delay(1000UL*60*timeBonusBreak);
    }
  }
  // Long break period
  digitalWrite(LED_BUILTIN, LOW);
  if (enableLongBreak == 1) {
    tone(pinSpeaker,beepFrequencyBreak,100);
    delay(300);
    tone(pinSpeaker,beepFrequencyBreak,100);
    delay(300);
    tone(pinSpeaker,beepFrequencyBreak,100);
    delay(1000UL*60*timeLongBreak);
  }
}
